# alacritty-configuration

Configuration and setup instruction for alacritty terminal

## Requirements

- alacritty (version 0.13 or higher)
- JetBrains Mono Font

## Setup

- Install alacritty and font
- Git clone
- Symlink alacritty folder from repo to `~/.config/`

## Replace Gnome's terminal with alacritty

- Install nautilus extension - [https://github.com/Stunkymonkey/nautilus-open-any-terminal](https://github.com/Stunkymonkey/nautilus-open-any-terminal)
- Run - `gsettings set com.github.stunkymonkey.nautilus-open-any-terminal terminal alacritty`
- Change terminal opening shortcuts if there's any
